<?php

namespace App\Console\Commands;

use App\Mail\firstReminderEmail;
use App\Mail\ReminderEmail;
use App\Mail\secondReminderEmail;
use App\Sessions;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends periodic reminders to user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d');
        $remindUsers = User::where('remind_date', '=', $now)->get();
        $allUsers = User::all();
        if (User::where('remind_date', '=', $now)->exists()) {
            foreach ($remindUsers as $remindUser) {
                Mail::to($remindUser->email)->send(new ReminderEmail());
            }
        }
        foreach ($allUsers as $user) {
            if($user->first_email_date == $now) {
                Mail::to($user->email)->send(new firstReminderEmail());
            } else if($user->second_email_date == $now) {
                Mail::to($user->email)->send(new secondReminderEmail());
            }
        }
    }
}
