<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
class SendYearlyUserReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'yearly:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends email to users who signed up for free, asking for donation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::where('patientcode', null)->where('role_id', '2')->orWhere('patientcode', 'like', '%'. 'GEN'.'%' )->get();

        foreach($users as $user) {
            if(Carbon::parse($user->created_at)->addYear() == Carbon::now()) {
                Mail::to($user->email)->send(new SendDonationRequest($user->firstname));
            }
        }
    }
}
