<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Sessions;
use App\User;
use App\Professional;
use App\Company;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUsers = User::all();
        $allSessions = Sessions::where('complete', 1);
        $activeUsers = Sessions::where('created_at', '>=' , Carbon::now()->subMonths(3))->get();
        return view('admin.index', compact('allUsers', 'allSessions', 'activeUsers'));
    }

    public function professionalIndex() 
    {
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first();   
        $profCount = Professional::all()->sum('used');
        $companyCodeCount = Company::where('id', $professional->company_id)->get()->first();
        $company = Company::where('contactemail', Auth::user()->email)->where('short_name', '!=', 'GEN')->get()->first();
  
        if($professional) {
            $users = User::where('professional_id', $professional->id)->where('consent', 1)->get();
            return view('admin.professional.index', compact('professional', 'users', 'companyCodeCount', 'company', 'profCount'));
        } else {
            return redirect()->back()->with('error', 'Email address not found');
        }
    }

    public function companyAdmin()
    {
        $company = Company::where('contactemail', Auth::user()->email)->get()->first();
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first();
        $allProfessionals = Professional::where('company_id', $professional->company_id)->get();     
        return view('admin.professional.admin.index', compact('professional', 'company', 'allProfessionals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
