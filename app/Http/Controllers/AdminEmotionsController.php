<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Emotion;
class AdminEmotionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allEmotions = Emotion::all();
    
        return view('admin.emotions.index', compact('allEmotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emotion = new Emotion();
        $emotion->name = $request->emotion;
        $emotion->save();
        return redirect()->back()->with('message', 'Emotion "'. $emotion->name . '" added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emotionName = Emotion::where('id', $id)->get()->pluck('name')->first();
        Emotion::findOrFail($id)->delete();
        return redirect()->back()->with('deleted', 'Emotion "'. $emotionName . '" removed');
    }
}
