<?php

namespace App\Http\Controllers;

use App\Professional;
use App\User;
use App\Company;

use Carbon\Carbon;

use App\Mail\WelcomeNewUser;
use App\Mail\SendHCProviderChangeMail;
use DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\newOwnerEmail;

class AdminProfessionalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allHealthcareProfessionals = Professional::where('deleted_at', null)->get();
        $allCompanies = Company::all();
        $healthcareProfessionals = array();
        foreach($allHealthcareProfessionals as $healthcareprofessional) {           
            $user = User::find($healthcareprofessional->user_id);
            $professional = [
                'id' => $healthcareprofessional->id,
                'firstname' => $user->firstname,
                'surname' => $user->surname,
                'email' => $user->email,
                'code' => $healthcareprofessional->code,
                'used' => $healthcareprofessional->used,
                'company_id' => $healthcareprofessional->company_id,
                'company' => $healthcareprofessional->company->name,
                'max_codes' => $healthcareprofessional->maximum_number_of_codes,
                'deleted_at' => $healthcareprofessional->deleted_at
            ];
            array_push($healthcareProfessionals, $professional);
        }
        return view('admin.healthcare.index', compact('healthcareProfessionals', 'allCompanies'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $user = User::create([
                'firstname' => $request['firstname'],
                'surname' => $request['surname'],
                'email' => $request['email'],
                'role_id' => 3
            ]); 
        } catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == '1062'){
                return redirect()->back()->with('delete', 'Sorry, email address already exists');
            }
        }
       

        $contactfirstname = $user->firstname;       
        $contactemail = $user->email;        

        $token = Str::random(64);

        DB::table('password_resets')->insert(['email' => $user->email, 'token' => bcrypt($token), 'created_at' =>  \Carbon\Carbon::now()->toDateTimeString()]);

        if($request->source === 'company'){
            $professional = new Professional;
            $professional->user_id = $user->id;
            $professional->code =  $request->prof_code . rand(100, 999);
            $professional->company_id =  $request->prof_comp_id;
            $professional->used = 0;
            $professional->save();

            Mail::to($contactemail)->send(new WelcomeNewUser($user, $token, $professional->code));
        } else if($request->source === 'adminCompany') {

            $company = new Company;
            $company->name = $request->companyname;
            $company->short_name = 'ST' . $request->shortname;
            $company->maximum_code_count = $request->codeCount;   
            $company->contactname = $request->firstname . ' '. $request->surname;
            $company->contactemail = $request->email;     
            $company->save();

            $companyName = Company::find($company->id);
            $professional = new Professional;
            $professional->user_id = $user->id;
            $professional->code = $companyName->short_name . rand(100, 999);
            $professional->company_id = $company->id;
            $professional->used = 0;
            $professional->save();            

            Mail::to($request->email)->send(new newOwnerEmail($request->firstname, $request->companyname, $request->codeCount, $token, $professional->code));
        } else {
            $companyName = Company::find($request->company);
            $professional = new Professional;
            $professional->user_id = $user->id;
            $professional->code = $companyName->short_name . rand(100, 999);
            $professional->company_id = $request->company;
            $professional->used = 0;
            $professional->save();

            Mail::to($contactemail)->send(new WelcomeNewUser($user, $token, $professional->code));
        }
        

        
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($request->company);

        $professional = Professional::find($id);        
        if($request->company != $professional->company_id) {
            $professional->code = $company->short_name . rand(100, 999);
        }        
        $professional->company_id = $request->company;        
        $professional->save();
        return redirect('/admin/professionals')->with('status', 'Healthcare Professional Amended!');
    }

    public function updateProf(Request $request, $id)
    {
        $user = User::find($id);
        $user->firstname = $request->firstname;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->save();
        return redirect()->back()->with('status', 'Healthcare Professional Amended!');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $professional = Professional::where('id', $id)->get()->first();
        $userRole = User::where('id', $professional->user_id)->pluck('role_id')->first();
       
        if($userRole == '3') {
            $users = User::where('professional_id', $id)->get();
  
            foreach($users as $user) {             
                $findUser = User::find($user->id);
                $findUser->patientcode = 'GEN' . rand(10000000, 99999999);
                $findUser->professional_id = 0;
                $findUser->save();

                Mail::to($findUser->email)->send(new SendHCProviderChangeMail($findUser->firstname, $findUser->patientcode));
            }
        } 
       
        $userToDelete = User::find($professional->user_id);
        $userToDelete->email = $userToDelete->email . Carbon::now()->timestamp;
        $userToDelete->save();
        $userToDelete->delete();

        Professional::find($id)->delete();
        return redirect()->back()->with('delete', 'Healthcare Professional Removed!');
    }
}
