<?php

namespace App\Http\Controllers;

use App\AdminResources;
use Illuminate\Http\Request;
use App\Resources;
use App\ResourceCategories;

class AdminResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allResources = Resources::all();
        $allCategories = ResourceCategories::orderBy('name', 'asc')->get();
        return view('admin.resources.index', compact('allResources', 'allCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resource = new Resources;
        $resource->link_text = $request->linkText;
        $resource->link_url = $request->linkUrl;
        $resource->category_id = $request->category;
        $resource->description = $request->description;
        $resource->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminResources  $adminResources
     * @return \Illuminate\Http\Response
     */
    public function show(AdminResources $adminResources)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdminResources  $adminResources
     * @return \Illuminate\Http\Response
     */
    public function edit(AdminResources $adminResources)
    {
     //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminResources  $adminResources
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Resources::findOrFail($id)->update($request->all());
        return redirect('/admin/resources')->with('status', 'Resource Amended!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminResources  $adminResources
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Resources::findOrFail($id)->delete();
        return redirect()->back()->with('delete', 'Resource Removed!');
    }
}
