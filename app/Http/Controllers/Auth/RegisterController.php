<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Condition;
use App\Professional;
use App\Company;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Mail\NewUser;
use App\Mail\SendLimitEmail;
use App\Mail\SendLimitEmailHCProf;

use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'dob' => ['required', 'integer', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'gender' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function index()
    {
        $allConditions = Condition::all();
        return view('newuser.register', compact('allConditions'));
    }

    protected function create(Request $data)
    {   
        if($data['patientcode']) {
            $professional = Professional::where('code', $data['patientcode'])->get();   
            if($professional->isEmpty()) {
                return redirect()->back()->with('error', 'Sorry, Access code not found, please re-check code');
            } else if($professional->first()->used == 20) {
                return redirect()->back()->with('error', "We're sorry, there has been an error with the code used. Please contact the healthcare professional who provided the code.");
            }
            $patientCode = $data['patientcode'] . rand(100, 999);
            $profId = $professional->pluck('id')->first();
        } else{
            $patientCode = 'GEN' . rand(10000000, 99999999);
            $profId = 0;
        }
    
        $user = User::create([
            'patientcode' => $patientCode,
            'firstname' => $data['firstName'],
            'surname' => $data['surname'],
            'dob' => Carbon::createFromFormat('d/m/Y', $data['dob'])->format('Y-m-d'),
            'email' => $data['email'],
            'password' => Hash::make($data['patientPassword2']),
            'gender' => $data['gender'],
            'trauma' => $data['conditionCheck'],
            'role_id' => 2,
            'consent' => $data['consent'] ?? 0,
            'professional_id' => $profId,
            'location' => $data['location']
        ]); 
        
        if($data['patientcode']) {
            if($professional->first()->company_id != null) { 
                $companyId = $professional->first()->company_id;
                $company = Company::find($companyId);
                $company->used += 1;
                $company->save();
            }            

            $professionalId = $professional->first()->id;
            $professional = Professional::find($professionalId);
            $professional->used += 1;
            $professional->save();
        }
        foreach($data['condition'] as $key => $value){
            $user = User::find($user->id);
            $condition = Condition::where('condition', $value)->get();
            if ($condition->isEmpty()) {
                $condition = new Condition;
                $condition->condition = $value;
                $condition->save();
                $conditionId = $condition->id;
            } else {
                $conditionId = $condition->pluck('id')->first();
            }              
            if($data['conditionCheck'] > 0) {
                $user->condition()->attach($conditionId, ['years' => $data['years'][$key], 'months' => $data['months'][$key]]);  
            }
        }        
        auth()->login($user);
        if($data['patientcode']) {
            if($professional->source === 'admin') {               
                if(($professional->used / $company->maximum_code_count) > 0.9) {
                    Mail::to('something@else.com')->send(new SendLimitEmail($company->name));
                }
            } else if($professional->source === 'app') {
                if($professional->used >= 18) {  
                    $user = User::where('id', $professional->user_id)->get();
                    Mail::to($user->first()->email)->send(new SendLimitEmailHCProf($user->first()->firstname));
                }
            }
        }
        // $conditions = $data['condition'];
        // Mail::to('something@else.com')->send(new NewUser($conditions));

        return redirect('/rateemotions');
    }

    protected function createProfessional(Request $data)
    {        
            $user = User::create([
                'firstname' => $data['firstName'],
                'surname' => $data['surname'],
                'email' => $data['email'],
                'password' => Hash::make($data['professionalPassword2']),
                'role_id' => 3
            ]); 
            $company = Company::create([
                'name' => $data['company'] != null ? $data['company'] : 'GEN' . rand(10000000, 99999999),
                'short_name' => 'GEN',
                'maximum_code_count' => 20,
                'used' => 0,
                'contactname' => $data['firstName'] . ' ' . $data['surname'],
                'contactemail' => $data['email']
            ]);
            $professional = Professional::create([
                'user_id' => $user->id,
                'code' => 'GEN' . rand(10000, 99999),
                'maximum_number_of_codes' => 20,
                'used' => 0,
                'company_id' => $company->id
            ]);
            
            auth()->login($user);
           
            return redirect('/dashboard');                
    }
}
