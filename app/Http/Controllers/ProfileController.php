<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Professional;
use Auth;
Use Carbon\Carbon;
use App\Company;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $professionalId = Professional::where('id', $user->professional_id)->get()->first();
        if($professionalId) {
            $professional = User::find($professionalId->user_id);
        } else {
            $professional = null;
        }
        return view('home.profile', compact('user', 'professional'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($request->id);   
        $user->firstname = $request->firstname;
        $user->surname = $request->surname;
        $user->dob = Carbon::parse($request->dob)->format('Y-m-d');
        $user->email = $request->email;
        $user->save();
        return redirect('/profile')->with('status', 'Profile Updated');
    }

    public function updateHealthcareProvider(Request $request, $id)
    {       
        if($request->patientcode == null) {    
            $user = User::find(Auth::user()->id);
            $user->patientcode = Auth::user()->patientcode;               
            $user->consent = $request->consent;
            $user->professional_id = Auth::user()->professional_id;
            $user->save(); 
            return redirect('/profile')->with('status', 'Profile Updated');
        } else {
            $professional = Professional::where('code', $request->patientcode)->get();
            if(!$professional->isEmpty()) {
                $user = User::find($request->id);   
                $user->patientcode = $request->patientcode == null ? $user->patientcode : $request->patientcode . rand(10000000, 99999999);               
                $user->consent = $request->consent;
                $user->professional_id = $professional != null ? $professional->pluck('id')->first() : 0;
                $user->save();            
    
                if($request->patientcode != null) {
                    $companyId = $professional->first()->company_id;
                    $company = Company::find($companyId);
                    $company->used += 1;
                    $company->save();
                } else {
                    $companyId = $professional->first()->company_id;
                    $company = Company::find($companyId);
                    $company->used -= 1;
                    $company->save();
                }                
                return redirect('/profile')->with('status', 'Profile Updated');
            } else {
                return redirect('/profile')->with('error', 'Sorry,HealthCare Professional Not Found');
            }
        }            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
