<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resources;
use App\ResourceCategories;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $categoryId = ResourceCategories::where('slug', $id)->get()->pluck('id')->first();
        $categoryName = ResourceCategories::where('slug', $id)->get()->pluck('name')->first();
        $allResources = Resources::where('category_id', $categoryId)->get();
        $allCategories = ResourceCategories::all()->sortBy('name');
        $category = ResourceCategories::where('id', $id)->pluck('name')->first();
        return view('home.resources', compact('allResources', 'allCategories', 'category', 'categoryName'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateClickCount()
    {
        $resourceId = $_POST['resource_id'];
        $resource = Resources::findOrFail($resourceId);
        $resource->click_count += 1;
        $resource->save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
