<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Professional;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Mail;
use App\Mail\SendHCProviderChangeMail;
use App\Mail\newOwnerEmail;

class adminCompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::all();
        return view('admin.companies.index', compact('allCompanies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = new Company;
        $company->name = $request->companyname;
        $company->short_name = 'ST' . $request->shortname;
        $company->maximum_code_count = $request->codeCount;   
        $company->contactname = $request->contactname;
        $company->contactemail = $request->contactemail;     
        $company->save();

        $token = Str::random(64);

        Mail::to($request->contactemail)->send(new newOwnerEmail($request->contactname, $request->companyname, $request->codeCount, $token));

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);
        $company->name = $request->companyname;
        $company->short_name = 'ST' . $request->shortname;
        $company->maximum_code_count =  $request->maximum_code_count;   
        $company->contactname = $request->contactname;
        $company->contactemail = $request->contactemail;
        $company->save();
        return redirect()->back()->with('status', 'Company Amended!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::findOrFail($id);
        $company->contactemail = $company->contactemail . Carbon::now()->timestamp;
        $company->save();
        $company->delete();

        $professionals = Professional::where('company_id', $id);
        $professionals->delete();

        foreach($professionals->get() as $professional) {     
            $users = User::where('professional_id', $professional->user_id)->get();
            foreach($users as $user) {
                $findUser = User::find($user->id);
                $findUser->patientcode = 'GEN' . rand(10000000, 99999999);
                $findUser->save();

                Mail::to($findUser->email)->send(new SendHCProviderChangeMail($findUser->firstname, $findUser->patientcode));
            }
            
        }
        return redirect()->back()->with('delete', 'Company Removed!');
    }
}
 