<?php

namespace App\Http\Controllers;
use App\ResourceCategories;
use Illuminate\Support\Str;

use Illuminate\Http\Request;

class adminResourceCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allResourceCategories = ResourceCategories::all();
        return view('admin.resourceCategories.index', compact('allResourceCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resourceCategory = new ResourceCategories;
        $resourceCategory->name = $request->name;
        $resourceCategory->slug = Str::slug($request->name);
        $resourceCategory->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resourceCategory = ResourceCategories::find($id);
        $resourceCategory->name = $request->resource_name;
        $resourceCategory->save();
        return redirect('/admin/resourceCategories')->with('status', 'Resource Category Name Amended!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ResourceCategories::findOrFail($id)->delete();
        return redirect()->back()->with('delete', 'Resource Category Removed!');
    }
}
