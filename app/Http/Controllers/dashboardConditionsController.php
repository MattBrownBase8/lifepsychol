<?php

namespace App\Http\Controllers;

use App\User;
use App\Tag;
use Auth;
use Illuminate\Http\Request;

class dashboardConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = Auth::user()->role_id == 1 ? 'admin.conditions.update' : 'dashboard.patients.update';
        $professional = Professional::where('user_id', Auth::user()->id)->get()->first();
        $user = User::find($id);
        $userConditions = $user->condition;
        $userConditionsArray = array();
        foreach($userConditions as $userCondition) {
            $tags = Tag::where('user_id', $id)->where('condition_id', $userCondition->id)->get()->pluck('tag');
            $condition = [
                'condition_id' => $userCondition->id,
                'condition' => $userCondition->condition,
                'tags' => $tags
            ];
            array_push($userConditionsArray, $condition);
        }
        return view('admin.conditions.show', compact('professional','userConditionsArray', 'user', 'path'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
