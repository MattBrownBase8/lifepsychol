<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeNewUser extends Mailable
{
    use Queueable, SerializesModels;

    public $firstname;
    public $token;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstname, $token, $code)
    {
        $this->firstname = $firstname;
        $this->token = $token;
        $this->code = $code;
        // $this->url = 'http://18.133.179.155/auth/passwordset/' . $token;
        $this->url = 'http://clientarea.base8innovations.com/lifepsychol/auth/passwordset/' . $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.welcomeNewUser')
                    ->with([
                        'name' => $this->firstname,                       
                        'url' => $this->url,
                        'code' => $this->code
                    ]);
    }
}
