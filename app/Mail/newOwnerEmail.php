<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newOwnerEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $contactName;
    public $companyname;
    public $codeCount;
    public $token;
    public $code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contactName, $companyname, $codeCount, $token, $code)
    {
        $this->contactName = $contactName;
        $this->companyname = $companyname;
        $this->codeCount = $codeCount;
        $this->token = $token;
        $this->code = $code;
        // $this->url = 'http://18.133.179.155/auth/passwordset/' . $token;
        $this->url = 'http://clientarea.base8innovations.com/lifepsychol/auth/passwordset/' . $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.welcomeNewOwner')
                    ->with([
                        'name' => $this->contactName,                       
                        'url' => $this->url,
                        'codeCount' => $this->codeCount,
                        'companyname' => $this->companyname,
                        'code' => $this->code
                    ]);
    }
}
