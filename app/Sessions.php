<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    protected $fillable = [
        'user_id', 'sessionKey'
    ];

    public function sessionvalue() 
    {
        return $this->hasMany(SessionValues::class, 'session_id');
    }

    public function totalScore()
    {
        $overallScore = 0;
        foreach ($this->sessionvalue as $value) {
            $overallScore += $value->score;
        }
        return $overallScore;
    }
}
