<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function condition()
    {
        $this->belongsTo(Condition::class);
    }
}
