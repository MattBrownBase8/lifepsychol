<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patientcode', 'firstname', 'surname', 'dob', 'email', 'password','gender', 'trauma', 'role_id', 'consent', 'professional_id', 'location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function emotions()
    {
        return $this->belongsToMany(Emotion::class)->withPivot('emotion_score');
    }

    public function values()
    {
        return $this->belongsToMany(Value::class);
    }

    public function session()
    {
        return $this->belongsTo(Value::class)->withPivot('id');
    }

    public function condition()
    {
        return $this->belongsToMany(Condition::class);
    }

    public function professional()
    {
        return $this->belongsTo(Professional::class)->withTrashed();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function CompanyProfessionals()
    {
        return $this->hasOne(Professional::class);
    }

}
