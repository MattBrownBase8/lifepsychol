<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Professional;
use Faker\Generator as Faker;

$factory->define(Professional::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\User::class),
        'code' => 'test',
        'company_id' => factory(App\User::class),
        'maximum_number_of_codes' => 0,
        'used' => 0,
        'source' => 'admin',       
    ];
});
