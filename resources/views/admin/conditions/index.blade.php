@extends('layouts.admin')
@section('content')
@if (Session::has('message'))
    <div class="alert alert-info text-center"><h2 class="mt-0 py-1">{{ Session::get('message') }}</h2></div>
@elseif (Session::has('deleted'))
    <div class="alert alert-danger text-center"><h2 class="mt-0 py-1">{{ Session::get('deleted') }}</h2></div>
@endif
<h1 class="text-center">Conditions</h1>
<div class="col-8 offset-2">
  <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
    <thead>
      <tr>            
          <th>Condition</th>  
          <th>User</th>
          <th>Tags</th>
      </tr>
    </thead>
    <tbody>    
      @foreach ($allConditions as $condition)    
      <tr>     
        <td>{{ $condition->condition }}</td>       
        <td>{{ $condition['users']['0']->name }}</td> 
        <td>bob</td> 
        {{-- <td>          
          <form-group>
            <button 
            class="btn btn-primary md-trigger" 
            type="button"
            data-emotion="{{ $emotion->name }}"       
            data-toggle="modal"
            data-target="#edit-emotion">Edit</button>
            <button 
            class="btn btn-danger md-trigger"
            type="button"
            data-id="{{ $emotion->id }}"
            data-toggle="modal"
            data-target="#delete-emotion">Delete</button>
        </form-group>
        </td>    --}}
      </tr>
      @endforeach  
    </tbody>
  </table>
</div>

{{-- edit question modal --}}
{{-- <div class="modal fade" id="edit-emotion" role="dialog">
  @include('includes.modals.editEmotionModal')
</div> --}}

{{-- delete question modal --}}
{{-- <div class="modal fade" id="delete-emotion" role="dialog">
  @include('includes.modals.deleteEmotionModal')
</div> --}}
@endsection



@section('scripts')
<script>
  $(document).ready( function () {
      $('#table1').DataTable();
  });
$('#edit-emotion').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let modal = $(this);
  let emotion = button.data('emotion');
  let id = button.data('id');      
  modal.find('.modal-content #emotion').val(emotion);
  const url = "{{ route('admin.emotions.update', ':id') }}"
  $('#editForm').attr("action", url.replace(':id', id));
});
$('#delete-emotion').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let modal = $(this);
    let id = button.data('id');
    const url = "{{ route('admin.emotions.delete', ':id') }}"
    $('#deleteForm').attr("action", url.replace(':id', id));
  })
</script>

@endsection
