@extends('layouts.admin')

@section('content')
    <div class="main-content container-fluid">
        <h3 class="pb-3">ADMIN PANEL  <br> <br> <small class="py-3">WELCOME! This is your personal dashboard where you will be able to access details about patients who sign up to the application. To ensure that you can see their information, you will need to ask patients to register for the app using the code you see at the top of this screen. Once registered, you will then be able to view patients details within the 'Patients' tab</small> </h3>
       
        @include('includes.admin.professional.stats.topNumbers')
        {{-- @include('includes.admin.stats.graphs') --}}
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            //-initialize the javascript
            App.init();
            App.dashboard();

        });
    </script>
    @endsection
