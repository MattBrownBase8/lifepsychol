@extends('layouts.admin')
@section('content')
    <div class="row main-content container-fluid">
        <div class="col-12">
            <h3 class="pb-5">Patients <br><br><small>This is where all the patients who have chosen to share their personal details will show. From here, you can chose to view notes on any Life Events the patient may have suffered, or you can view how your patients have scored each of their sessions.</small></h3>
        </div>
    </div>
   
    <div class="col-12">
        <table class="table table-striped table-hover table-fw-widget text-center darkTableHead" id="table1">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="20%">Email</th>
                <th width="10%">Date of Birth</th>               
                <th width="10%">Gender</th>
                <th width="20%">Life Event</th>     
                <th width="20%">Sessions</th>           
            </tr>
            </thead>
            <tbody>                
            @foreach ($allUsers as $user)        
                <tr>
                    <td>{{ $user->firstname . ' ' .  $user->surname }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ \Carbon\Carbon::parse($user->dob)->format('d/m/Y') }}</td>                    
                    <td>{{ ($user->gender == 1) ? 'Male' : (($user->gender == 2) ? 'Female' : 'Prefer Not to Say' ) }}</td>
                    @if($user->trauma != null)
                    <td><a href="{{ route('dashboard.patients.show', $user->id) }}">Click to view Life Event</a></td>  
                    @else 
                    <td>No Life Event Recorded</td>  
                    @endif
                    <td><a href="{{ route('dashboard.sessions.show', $user->id) }}">Click to view</a></td>                  
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });    
    </script>
@endsection
