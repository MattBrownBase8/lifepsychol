@extends('layouts.admin', [
    'title' => 'Patient Scores'
])
@section('content')
<div class="row main-content container-fluid">
    <div class="col-12">
        <h3 class="pb-5">{{ session('user')->firstname .' '. session('user')->surname }}'s Sessions <br><br><small>Here is a break down of how your patient has scored each of their chosen values this session. You can also view their notes to see how they were feeling this day.</small></h3>
    </div>
</div>
    <div class="col-8 offset-2">
        <table class="table table-striped table-hover table-fw-widget text-center" id="table1">
            <thead>
            <tr>
                <th width="20%">Value</th>    
                <th width="20%">Score</th>
                <th width="60%">Note</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($allScores as $score)
                <tr>
                    <td>{{ $score->value->name }}</td> 
                    <td>{{ number_format($score->score, 2) }}</td>  
                    <td>{{ $score->note ? $score->note : 'No note left' }}</td>                           
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-4 offset-4 pb-5">
            <a href="{{ url()->previous() }}" class="btn btn-primary btn-block">
                BACK
            </a>
        </div>
    </div>
    {{-- <row class="py-5">
    @foreach ($allScores as $key => $score)
        <div class="col-8 offset-2">
            <button class="btn profScore mt-5 btn-block" data-toggle="collapse" data-target="#multiCollapseExample{{$key}}" aria-expanded="false" aria-controls="multiCollapseExample{{$key}}">
                <h2 class="my-0">{{ $score->value->name }}</h2>
            </button>
            <div class="collapse multi-collapse" id="multiCollapseExample{{$key}}">
                <div class="card card-body profScoreNote mt-4 text-center">
                    <h3 class="mt-0">Score: {{ number_format($score->score, 2) }} <br></h3>
                    {{ $score->note != null ? $score->note : 'No note left' }}
                </div>
            </div>
        </div>
     @endforeach
    </row>
    <row class="pt-5">
        <div class="col-8 offset-2">            
            <a class="btn profScore mt-5 btn-block" href="{{ route('dashboard.sessions.show', session('user')->id) }}"><h2 class="my-0">Back</h2></a>                  
        </div>
    </row> --}}
@endsection
@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });    
    </script>
@endsection
