@extends('layouts.admin')
@section('content')
    <h1 class="text-center pt-5">Users
    </h1>
    <div class="col-12">
        <table class="table table-striped table-hover table-fw-widget" id="table1">
            <thead>
            <tr>
                <th width="10%">Patient Code</th>  
                <th width="10%">Life Event</th>  
                <th width="10%">DOB</th>
                <th width="20%">Professional Assigned To</th>
                <th width="20%">Location</th>
                {{-- <th width="20%">Actions</th> --}}
            </tr>
            </thead>
            <tbody>
            @foreach ($allUsers as $key=>$user)
                <tr>
                    <td>{{ $user->patientcode }}</td>    
                    <td><a href="{{ route('admin.conditions.show', $user->id) }}">Click to View</a></td>                
                    <td>{{ \Carbon\Carbon::parse($user->dob)->format('d/m/Y') }}</td>
                    <td>{{ $user->professional_id === 0 ? 'Not Assigned' : $user->professional->code }}</td>
                    <td>{{ $user->location ? $user->location : 'Not Given' }}</td>                    
                    {{-- <td>
                        <form-group>
                            <button
                                class="btn btn-primary md-trigger"
                                type="button"
                                data-id="{{ $user->id }}"
                                data-toggle="modal"
                                data-target="#edit-user">Edit</button>
                            <button
                                class="btn btn-danger md-trigger"
                                type="button"
                                data-id="{{ $user->id }}"
                                data-toggle="modal"
                                data-target="#delete-user">Delete</button>
                        </form-group>
                    </td> --}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="edit-user" role="dialog">
        @include('includes.modals.editUserModal')
    </div>
    <div class="modal fade" id="delete-user" role="dialog">
        @include('includes.modals.deleteModal')
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            $('#table1').DataTable();
        });
        $('#edit-user').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let companyname = button.data('companyname');
            let code = button.data('code');
            let shortname = button.data('shortname');
            let contactname = button.data('contactname');
            let contactemail = button.data('contactemail');
            let maximum_code_count = button.data('maximum_code_count');
            let id = button.data('id');
            modal.find('.modal-content #companyname').val(companyname);
            modal.find('.modal-content #code').val(code);
            modal.find('.modal-content #shortname').val(shortname);
            modal.find('.modal-content #contactname').val(contactname);
            modal.find('.modal-content #contactemail').val(contactemail); 
            modal.find('.modal-content #maximum_code_count').val(maximum_code_count);                       
            const url = "{{ route('admin.companies.update', ':id') }}"
            $('#editForm').attr("action", url.replace(':id', id));
        });
        $('#delete-user').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let modal = $(this);
            let id = button.data('id');
            const url = "{{ route('admin.users.delete', ':id') }}"
            $('#deleteForm').attr("action", url.replace(':id', id));
        })
    </script>

@endsection


