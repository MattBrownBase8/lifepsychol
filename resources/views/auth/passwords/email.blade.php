@extends('layouts.newuser')

@section('content')
<div class="container pt-5">
    <div class="row justify-content-center pt-5">
        <div class="col-md-12 pt-5">            
            <div class="row">
                <div class="profileBoxLeft col">
                    <div class="row">
                        <div class="col-12 py-5">
                            <h2 class="text-center">{{ __('Reset Password') }}</h2>
                            @if (session('status'))
                                <div class="alert alert-success text-center" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                    <div class="col-md-4">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-3">
                                        <a href="{{ url('/home') }}" class="btn btn-danger btn-block" role="button">Cancel</a>
                                    </div> 
                                    <div class="col-md-3 text-center">
                                        <button type="submit" class="btn btn-success btn-block">
                                            {{ __('Send Password Reset Link') }}
                                        </button>                                        
                                    </div>      
                                                                
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
    </div>
</div>
@endsection
