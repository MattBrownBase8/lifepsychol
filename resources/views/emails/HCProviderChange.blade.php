@component('mail::message')
Hi {{ $name }},
    Your Healthcare provider has changed. Please be aware, your code has now been changed to {{ $code }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
