@component('mail::message')
Hi, {{ $name }}. We notice that you have been using {{ config('app.name') }} for a year now and were wondering if you could make a donation of £10. Please visit <a href="#">our website</a> to make the donation.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
