@component('mail::message')
# Welcome {{ $name }}

You have now been registered for Smart Tracker as both an owner of {{ $companyname }} and a healthcare professional. {{ $companyname }} has been given {{ $codeCount }} codes to give to your patients. 

Your individual code is <br> <h1>{{ $code }}</h1> <br> If you wish to sign any patients up as your own patients, please give them this code when they are ready to sign up.  <br>

Please click the link below to set your account password and get access to your account :

@component('mail::button', ['url' => $url])
Reset Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
