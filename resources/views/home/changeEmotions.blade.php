@extends('layouts.app', [
    'title' => 'Change Emotions'
])
@section('content')
    <div class="container">       
        <div class="px-md-4 px-2">
            <div class="userInstruction">
                To replace one of the emotion words you previously selected to use, please deselect the one you no longer need and replace it with an alternative word. When you select the word you want, please ensure you score it positive, negative or neutral.
                <div class="d-block d-md-none">
                    To remove an emotion in order to add another, please click on the minus button next to the emotion and click the plus button to add
                </div>
    
            </div>
            @if(\Session::has('success'))
                <div class="alert alert-success mt-1">
                    {!! \Session::get('success') !!}
                </div>
            @endif
            <h2 class="text-center pt-2"><span class="newEmotionScores">18</span>/18 Selected</h2>          
            <div class="d-none d-md-block stickyHeader">
                <div class="row pt-3">
                    <div class="col-2 offset-6 offset-md-4 text-center pl-0" >
                        Positive
                    </div>
                    <div class="col-2 text-center" style="padding-left: 10px;">
                        Neutral
                    </div>
                    <div class="col-2 text-center" style="padding-left: 20px;">
                        Negative
                    </div>
                </div>
            </div>
            <div class="d-block d-md-none">
                <div class="row pt-3">
                    <div class="col-2 offset-4 text-center pl-4">
                        +ve
                    </div>
                    <div class="col-2 text-center pl-3">
                        Neutral
                    </div>
                    <div class="col-3 text-center pl-1">
                        -ve
                    </div>
                </div>
            </div>
            <form action="{{ route('changeemotions.update', $userId) }}" method="POST" id="emotionsForm">
                @csrf
                @foreach( $allEmotions as $emotion )              
                    <div class="row pt-2">
                        @if(in_array($emotion->id, $scoredEmotionsIds) && $scoredEmotions[$emotion->id] == 1)
                            <div class="col-4 col-md-4 registerText" style="color: #8bc53f">{{ ucfirst($emotion->name) }}</div>
                        @elseif(in_array($emotion->id, $scoredEmotionsIds) && $scoredEmotions[$emotion->id] == -1)
                            <div class="col-4 col-md-4 registerText" style="color: #ec1c24">{{ ucfirst($emotion->name) }}</div>
                        @elseif(in_array($emotion->id, $scoredEmotionsIds) && $scoredEmotions[$emotion->id] == 0)
                            <div class="col-4 col-md-4 registerText" style="color: #f6921e">{{ ucfirst($emotion->name) }}</div>
                        @else
                            <div class="col-4 col-md-4 registerText">{{ ucfirst($emotion->name) }}</div>
                        @endif
                        <div class="col-2 pr-0 text-center">
                            <input
                                type="radio"
                                id="emotionCheckbox"
                                name="emotions[{{ $emotion->id }}]"
                                class="registerInputRadio radio"
                                value='1'
                                {{in_array($emotion->id, $scoredEmotionsIds)?($scoredEmotions[$emotion->id] == 1 ? 'checked':'') : ''}}
                                {{array_key_exists($emotion->id, $archivedEmotions)?($archivedEmotions[$emotion->id] == "1" ? 'disabled':'') : ''}}>
                        </div>
                        <div class="col-2 pr-0 text-center">
                            <input
                                type="radio"
                                id="emotionCheckbox"
                                name="emotions[{{ $emotion->id }}]"
                                class="registerInputRadio radio"
                                value='0'
                                {{in_array($emotion->id, $scoredEmotionsIds)?($scoredEmotions[$emotion->id] == 0 ? 'checked':'') : ''}}
                                {{array_key_exists($emotion->id, $archivedEmotions)?($archivedEmotions[$emotion->id] == "1" ? 'disabled':'') : ''}}>
                        </div>
                        <div class="col-2 pr-0 text-center">
                            <input
                                type="radio"
                                id="emotionCheckbox"
                                name="emotions[{{ $emotion->id }}]"
                                class="registerInputRadio radio"
                                value='-1'
                                {{in_array($emotion->id, $scoredEmotionsIds)?($scoredEmotions[$emotion->id] == -1 ? 'checked':'') : ''}}
                                {{array_key_exists($emotion->id, $archivedEmotions)?($archivedEmotions[$emotion->id] == "1" ? 'disabled':'') : ''}}>
                        </div>
                        <div class="col-2 d-none d-md-block">
                            <input
                                type="hidden"
                                id="archived"
                                name="archived[{{ $emotion->id }}]"
                                value={{array_key_exists($emotion->id, $archivedEmotions)?($archivedEmotions[$emotion->id] == "1" ? '1':'0') : '0'}}>
    
                            <span class="remove" name="{{ $emotion->id }}">Remove</span> / <span class="add" name="{{ $emotion->id }}">Add</span>
                        </div>
                        <div class="col-2 d-block d-md-none text-center pr-0">
                            <span class="remove" name="{{ $emotion->id }}">-</span> / <span class="add" name="{{ $emotion->id }}">+</span>
                        </div>
                    </div>                    
                @endforeach
                <h2 class="text-center pt-3"><span class="newEmotionScores">18</span>/18 Selected</h2>   
                <p class="pt-4" style="font-size: 15px">Not seeing your emotion? <a href="{{ url('/addemotionsuser', $value) }}">Click here</a></p>
                <div class="row pt-4">
                    <div class="col-12 form-group" id="submitButton">
                       <button href="#" class="btn btn-primary btn-block loginButton" type="submit">Next</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Scoring Emotions</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Please score the emotions listed here according to how you perceive each emotion, whether it is positive, negative or in between.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="emotionsModal" tabindex="-1" role="dialog" aria-labelledby="emotionsModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="row pt-3">
                            <div class="col-12 text-center">
                                <h4>Please select 18 emotions to continue.</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("input:radio").on('click', function() {
            var $box = $(this);
            if ($box.is(":checked")) {
                var group = "input:radio[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);                
            } else {
                $box.prop("checked", false);
            }
            updateCount();
        });
        $(".remove").each(function(_, remove) {
            $(remove).on("click", function(){
                var $box = $(this);
                var id = $box.attr("name");
                var group = "input:radio[name='emotions[" + id + "]']";
                if ($(group).is(":checked")) {
                    $(group).prop("disabled", true);
                    $('input[id="archived"][name="archived[' + id + ']"]').val("1");
                } else {
                    $(group).prop("disabled", false);
                }

                updateCount();
            });
        });
        $(".add").each(function(_, remove) {
            $(remove).on("click", function(){
                var $box = $(this);
                var id = $box.attr("name");
                var group = "input:radio[name='emotions[" + id + "]']";
                if ($(group).is(":checked")) {
                    $(group).prop("disabled", "");
                    $('input[id="archived"][name="archived[' + id + ']"]').val("0");
                } else {
                    $(group).prop("disabled", "");
                }
                updateCount();
            });
        });
        $( "#emotionsForm" ).submit(function( event ) {
            let checkboxList = $("input:radio[id=emotionCheckbox]:checked").length;
            let archivedEmotions = $('input:hidden[id=archived]');
            archivedEmotions = archivedEmotions.filter(function (){ return $(this).val() === '1';}).length;
            if ((checkboxList - archivedEmotions) < 18 || (checkboxList - archivedEmotions) > 18) {
                event.preventDefault();
                $('#emotionsModal').modal();
            } else {
                // $( "#emotionsForm" ).submit();
                updateCount();
            }
        });
        function updateCount() {
            let checkboxList = $("input:radio[id=emotionCheckbox]:checked").length;
            let archivedEmotions = $('input:hidden[id=archived]');
            archivedEmotions = archivedEmotions.filter(function (){ return $(this).val() === '1';}).length;
            let count = (18 - (checkboxList - archivedEmotions));
            $('.newEmotionScores').html(checkboxList - archivedEmotions);
            if(count === 0) {
                $("#submitButton").html('<button href="#" class="btn btn-primary btn-block loginButton" type="submit">Next</button>');
            } else {
                $("#submitButton").html('<button href="#" class="btn btn-primary btn-block loginButton" disabled>Please select ' + count + ' more to continue</button>');
            }
        }
        $(document).ready(function(){
            $(".remove").css('cursor', 'pointer');
            $(".add").css('cursor', 'pointer');
            
        })
    </script>
@endsection
