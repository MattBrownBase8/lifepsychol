@extends('layouts.home')
@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col-12">
            <h3>Today's Summary</h3>
        </div>
    </div>
    <div class="row px-2 px-md-5 pt-4">
        <div class="col-6 darkText font-weight-bold pl-0">
            <h2>Today</h2>
        </div>
        <div class="col-6 pr-0 darkText font-weight-bold">
            <h2>{{ \Carbon\carbon::parse($oldDate)->format('d M Y') }}</h2>
        </div>
    </div>
    <div class="row px-2 px-md-5">
        <div class="col-6">
            <div class="row">
                @foreach ($currentSessionValues as $value)
                @php
                    $count = $value->emotions->count();
                    $total = $count * 2;
                    $marker = ((($value->score * $count) + $count) / $total) * 100;                  
                @endphp
                    <h4 class="pt-2 summaryHeaders">{{ $value->value->name }}</h4>
                    <div class="col-12 sliderBarLarge d-none d-md-block" style="padding-left: {{ $marker > 95 ? $marker-4 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                    <div class="col-12 sliderBarLarge d-block d-md-none" style="padding-left: {{ $marker > 88 ? $marker-12 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-6">
            <div class="row pl-3">
                @foreach ($oldSessionValues as $value)
                @php
                    $count = $value->emotions->count();
                    $total = $count * 2;
                    $marker = ((($value->score * $count) + $count) / $total) * 100;                    
                @endphp
                    <h4 class="pt-2 summaryHeaders"><a href="{{ route('summarydetails', $value->id )}}">{{ $value->value->name }}</a></h4>
                    <div class="col-12 sliderBarLarge d-none d-md-block" style="padding-left: {{ $marker > 95 ? $marker-4 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                    <div class="col-12 sliderBarLarge d-block d-md-none" style="padding-left: {{ $marker > 88 ? $marker-12 : $marker }}%">
                        <div class="slider shadow">
                            &nbsp
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="row px-md-5 pt-4">
        <div class="col-12 col-md-4 text-center pb-2 px-2">
            <a href="{{ url('/home') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Home</a>
        </div>
        <div class="col-12 col-md-4 text-center pb-2 px-2">
            <a href="{{ url('/compare') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Choose Another Date</a>
        </div>
        <div class="col-12 col-md-4 text-center pb-2 px-2">
            <a href="{{ route('charts') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">View Trends</a>
        </div>
    </div>
    
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Comparing results</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               Here you can see how your current scores match up against ones recorded previously to see how well you're doing each time.
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
