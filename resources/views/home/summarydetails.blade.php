@extends('layouts.home')
@section('content')
<div class="container">
    <div class="row text-center">
        <div class="col-12">
            <h3>Today's Summary</h3>
        </div>
    </div>
    <div class="row px-2 px-md-5 pt-2 text-center darkText">
        <div class="col-12">
            <h2 class="font-weight-bold pb-3">{{ $valueName }}</h2>
        </div>
    </div>
    <div class="row px-2 px-md-5">
        @foreach ($emotions as $emotion)
        <div class="col-4 text-center">
            <p class="emotionCircleSelected">{{ $emotion->name }}</p>
        </div>
        @endforeach
    </div>
    <div class="row px-2 px-md-5">
        <div class="col-12">
            <form action="">
                <textarea name="" id="" cols="30" rows="6" class="form-control noteInputBoxBorder pt-3">{{ $note }}</textarea>
            </form>
        </div>
    </div>
    <div class="row px-2 px-md-5 pt-4 text-center">
        <div class="col-4 pt-3 pr-0">
            <h2>Score:</h2>
        </div>
        <div class="col-4 pl-0">
            <h1 class="scoreCircle pb-3 text-center">{{ round($valueScore, 1) }}</h1>
        </div>
        <div class="col-4 pt-2">
            <a href="{{ url('/overallsummary') }}" class="btn btn-primary btn-block loginButton" role="button" style="background-color: #344EA9; border: none;">Back</a>
        </div>
    </div>
    <div class="modal fade" id="helpModalHome" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Value details</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               Here you can see how you scored each value, along with the notes left against that emotion.
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
</div>
@endsection
