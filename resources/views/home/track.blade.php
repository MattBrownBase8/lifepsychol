@extends('layouts.home',[
    'title' => 'Track Values'
    ])
@section('content')
<div class="container">
  <div class="row text-center">
    <div class="col-12">
      <div class="pt-3 trackValueWording">How are you feeling about your...</div>
      <div style="font-weight: 800" class="py-2 trackValueWording">{{ $allValuesName }} <br>({{ $index }} of {{ $allValuesCount}})</div>
      <div class="pb-3 trackValueWording">Please select between 1 and 9 relevant emotions</div>
    </div>
  </div>
  <form action="{{ route('trackemotions.store') }}" method="POST" id="emotionsForm" autocomplete="off">
    @csrf
    <div class="row px-3 text-center">
      <input type="hidden" name="pageNumber" value="{{ $index }}">
      <input type="hidden" name="valueId" value="{{ $allValuesId }}">
      @foreach ($emotions as $emotion)
        <div class="col-4 col-md-2 px-0 text-center emotionCheckBox" name="{{ $emotion->emotion_score }}">
          <input type="checkbox" name="emotions[{{ $emotion->id }}]" id="checked">
          <p class="emotionCircle" id="emotionCircle{{ $emotion->id }}" onclick="changeColour({{ $emotion->id }})">{{ $emotion->name }}</p>
        </div>
      @endforeach
      <div class="col-12 col-md-6 offset-md-3 pt-md-3">
        <h5>Want to select a different emotion? <a href="{{ route('changeemotions', ['id' => Auth::user()->id, 'valueId' => $index]) }}"> <br> Click here</a></h5>
      </div>
    </div>
    <div class="row px-5 pt-4">
      <div class="col-12">
        <button href="#" class="btn btn-primary btn-block loginButton">Next</button>
      </div>
    </div>
  </form>
</div>

<div class="modal fade" id="helpModalHome" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Scoring your values</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Here is where you can add between 1 and 9 emotions which best describe your feelings about the given emotion.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="emotionsSmallModal" tabindex="-1" role="dialog" aria-labelledby="valuesModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row pt-5">
          <div class="col-12 text-center">
            <h4>Please select between 1 and 9 emotions to continue.</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="emotionsLargeModal" tabindex="-1" role="dialog" aria-labelledby="valuesModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="row pt-5">
            <div class="col-12 text-center">
                <h4>Please select 9 or fewer emotions to continue.</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
              <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">OK</button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
  <script>
    $(function(){
      $('.emotionCheckBox').click(function(){
        var checkbox = $(this).find('input');
        var checked = checkbox.is(':checked');
        if (checked) {
          checkbox.prop('checked', false);
        } else {
          checkbox.prop('checked', true);
        }
      });
    });
    function changeColour(id) {
      let checked = $('[name="emotions['+id+']" ]').is(':checked');
      if (checked) {
        $('#emotionCircle' + id).removeClass('emotionCircleSelected');
      } else {
        $('#emotionCircle' + id).addClass('emotionCircleSelected');
      }
    }
    $( "#emotionsForm" ).submit(function( event ) {
      let $checkedEmotionsLength = $("input:checkbox[id=checked]:checked").length;
      if ( $checkedEmotionsLength < 1) {
        event.preventDefault();
        $('#emotionsSmallModal').modal();
      } else if ($checkedEmotionsLength > 9){
        event.preventDefault();
        $('#emotionsLargeModal').modal();
      }else {
        $( "#emotionsForm" ).submit();
      }
    });
  </script>
@endsection
