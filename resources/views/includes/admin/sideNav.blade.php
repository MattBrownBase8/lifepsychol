@if(Auth::user()->role_id == 1)
<div class="be-left-sidebar">
@else
<div class="be-left-sidebar be-left-sidebar-prof">    
 @endif
  <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="index.html#">Dashboard</a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
          <li class="divider"></li>
         {{-- <li>
            <a href="{{ route('home') }}">
              <i class="icon mdi mdi-arrow-left"></i>
              <span>Back To Site</span>
            </a>
          </li> --}}
          @if(Auth::user()->role_id == 1)
          <li>
              <a href="{{ route('admin') }}">
                  <i class="icon mdi mdi-home"></i>
                  <span>Home</span>
              </a>
          </li>
           <li>
            <a href="{{ route('admin.resources') }}">
              <i class="icon mdi mdi-bookmark"></i>
              <span>Resources</span>
            </a>
          </li>          
          <li>
            <a href="{{ route('admin.resourceCategories') }}">
              <i class="icon mdi mdi-bookmark"></i>
              <span>Resource Categories</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.emotions') }}">
              <i class="icon mdi mdi-mood"></i>
              <span>Emotions</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.users') }}">
              <i class="icon mdi mdi-accounts-alt"></i>
              <span>Users</span>
            </a>
          </li>
          <li>
            <a href="{{ route('admin.companies') }}">
              <i class="icon mdi mdi-account-box-mail"></i>
              <span>Companies</span>
            </a>
          </li> 
          <li>
            <a href="{{ route('admin.professionals') }}">
              <i class="icon mdi mdi-account-box-mail"></i>
              <span>Healthcare Professionals</span>
            </a>
          </li> 
          @endif   
          @if(Auth::user()->role_id == 3)   
          <li>
            <a href="{{ route('dashboard') }}">
              <i class="icon mdi mdi-home"></i>
              <span>Home</span>
            </a>
          </li>
          <li>
            <a href="{{ route('dashboard.patients.index') }}">
              <i class="icon mdi mdi-account-box-mail"></i>
              <span>Patients</span>
            </a>
          </li> 
            @if($company)
            <li>
              <a href="{{ route('dashboard.admin') }}">
                <i class="icon mdi mdi-account-box-mail"></i>
                <span>Admin</span>
              </a>
            </li>
            @endif
          @endif        
        </div>
      </div>
    </div>
  </div>
</div>
