<div class="row">
    <div class="col-12 col-lg-4">
        <div class="card card-table">
            <div class="card-header">
                <div class="title">Total Number of People By Gender</div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-borderless text-center">
                    <thead>
                    <tr>
                        <th style="width:50%;">Gender</th>
                        <th>Count</th>
                    </tr>
                    </thead>
                    <tbody class="no-border-x">
                    <tr>
                        <td>Male</td>
                        <td>{{ $allUsers->where('gender', 1)->count() }}</td>
                    </tr>
                    <tr>
                        <td>Female</td>
                        <td>{{ $allUsers->where('gender', 2)->count() }}</td>
                    </tr>
                    <tr>
                        <td>Other/Prefer not to say</td>
                        <td>{{ $allUsers->where('gender', 3)->count() }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-4">
        <div class="card card-table">
            <div class="card-header">
                <div class="title">Age Ranges of Patients</div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-borderless text-center">
                    <thead>
                    <tr>
                        <th style="width:50%;">Age</th>
                        <th>Count</th>
                    </tr>
                    </thead>
                    <tbody class="no-border-x">
                    <tr>
                        <td>10-20</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(20), \Carbon\Carbon::now()->subYear(10)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>21-30</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(30), \Carbon\Carbon::now()->subYear(20)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>31-40</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(40), \Carbon\Carbon::now()->subYear(30)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>41-50</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(50), \Carbon\Carbon::now()->subYear(40)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>51-60</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(60), \Carbon\Carbon::now()->subYear(50)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>61-70</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(70), \Carbon\Carbon::now()->subYear(61)])->count()}}</td>
                    </tr>
                    <tr>
                        <td>71-80</td>
                        <td>{{$allUsers->whereBetween('dob', [\Carbon\Carbon::now()->subYear(80), \Carbon\Carbon::now()->subYear(71)])->count()}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card card-table">
            <div class="card-header">
                <div class="title">Life Event Selected</div>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-striped table-borderless text-center">
                    <thead>
                    <tr>
                        <th style="width:50%;">Life Event?</th>
                        <th>Count</th>
                    </tr>
                    </thead>
                    <tbody class="no-border-x">
                    <tr>
                        <td>Yes</td>
                        <td>{{ $allUsers->where('trauma', 1)->count() }}</td>
                    </tr>
                    <tr>
                        <td>No</td>
                        <td>{{ $allUsers->where('trauma', 0)->count() }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
