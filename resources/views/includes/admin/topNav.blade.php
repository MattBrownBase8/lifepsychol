@if(Auth::user()->role_id == 1)
<nav class="navbar navbar-expand fixed-top be-top-header">
@else
<nav class="navbar navbar-expand fixed-top be-top-header be-top-header-prof adminHeader">
@endif
  <div class="container-fluid">
    @if(Auth::user()->role_id == 1)
    <div class="be-navbar-header">
      <h3 class="text-center">Smart Tracker Admin Panel</h3>      
    </div>
    @else
    <div class="be-navbar-header">
      <div class="row">
        <div class="col-6 offset-3 text-center">
          <h3 class="text-center">Smart Tracker</h3>  
        </div>
      </div>    
    </div>
    @endif
    <div class="page-title">     
      <span>Welcome {{ Auth::user()->firstname }}
        @if(Auth::user()->role_id == 3)
         - Your code is {{ $professional->code }}  
        @endif
      </span>        
    </div>
    <div class="text-right pr-5">
      <a href="{{url('/logout')}}">Logout</a>
    </div>  
  </div>
</nav> 