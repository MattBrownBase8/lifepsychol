<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content text-center">      
      <div class="modal-body text-center mt-3">
        <h4>Are you sure you do not wish to share your information with your healthcare provider? This application will work without sharing your details, but to get the most out of your time on this application, sharing your details is the best way to work with your healthcare professional to reach your goals.</h4>
        <div class="modal-footer">
          <button class="btn btn-success" type="submit" data-dismiss="modal">Go Back</button>
          <button class="btn btn-primary" type="submit" onClick='submitWithoutConsent()'>Continue without sharing</button>
        </div>
      </div>
    </div>
  </div>
  