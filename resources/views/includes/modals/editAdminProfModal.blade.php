<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content px-5">
    <div class="modal-header modal-header-colored px-0">
      <h3 class="modal-title">Edit Healthcare Professional</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="firstname"><h4>First Name</h4></label>
              <input class="form-control" type="text" name="firstname" id="firstname">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="surname"><h4>Last Name</h4></label>                            
              <input class="form-control" type="text" name="surname" id="surname">
            </div>
          </div> 
        </div>        
        <div class="row">
          <div class="col-12">
            <label for="email"><h4>Email</h4></label>                            
            <input class="form-control" type="email" name="email" id="email">
          </div>        
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-success modal-close" type="submit">Submit</button>
        </div>
    </form>
  </div>
</div>
