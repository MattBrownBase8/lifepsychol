<div class="modal-dialog modal-dialog-centered" role="document">
  <div class="modal-content px-5">
    <div class="modal-header modal-header-colored px-0">
      <h3 class="modal-title">Edit Company</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="companyname"><h4>Company Name</h4></label>
              <input class="form-control" type="text" name="companyname" id="companyname">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="shortname"><h4>Company Short Name</h4></label>                            
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">ST</div>
                    </div>
                    <input class="form-control" type="text" name="shortname" id="shortname">
                </div>
            </div>
          </div> 
        </div>         
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="contactname"><h4>Contact Name</h4></label>
              <input class="form-control" type="text" name="contactname" id="contactname">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label for="contactemail"><h4>Contact Email</h4></label>
              <input class="form-control" type="text" name="contactemail" id="contactemail">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <label for="maximum_code_count"><h4>Maximum Number of Codes</h4></label>
              <input class="form-control" type="text" name="maximum_code_count" id="maximum_code_count">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-success modal-close" type="submit">Submit</button>
        </div>
    </form>
  </div>
</div>
