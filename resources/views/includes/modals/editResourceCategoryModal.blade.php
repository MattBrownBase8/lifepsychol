<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Resource</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <div class="modal-body form"> 
        <div class="row">
          <div class="col-12">
            <div class="form-group">
                <label for="resourceName"><h4>Resource Name</h4></label>
                <input class="form-control" type="text" name="resource_name" id="resource_name">
            </div>
          </div>
        </div>    
      </div> 
      <div class="modal-footer">
        <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-success modal-close" type="submit">Submit</button>
      </div>
    </form>
  </div>
</div>
