<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header modal-header-colored">
      <h3 class="modal-title">Edit Tags</h3>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  <form id="editForm" action="" method="post">
    @method('put')
      {{ csrf_field() }}
      <input type="hidden" id="user_id" value="" name="user_id">
      <input type="hidden" id="condition_id" value="" name="condition_id">
      <div class="modal-body form">
        <div class="row">
          <div class="col-12">
              <div class="form-group"><label><h4>Tags. If entering multiple, separate with a comma</h4></label>
                  <textarea class="form-control" type="text" name="tags" id="tags"></textarea>
              </div>
          </div>          
        </div>  
        <div class="modal-footer">
          <button class="btn btn-secondary modal-close" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-success modal-close" type="submit">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>
