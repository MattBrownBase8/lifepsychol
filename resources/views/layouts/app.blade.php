<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/img/smiley-face.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/smiley-face.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/smiley-face.png') }}">
    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{-- Date Picker --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- Select2 --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
</head>
<body>
    <div class="sideMenu pt-3" id="sideMenu" style="display: none">
        <div class="pr-3 text-right">
            <h1 id="closeMenu">x</h1>
        </div>
        <ul class="sideMenuList pr-4">
            <li class="pb-3"><a href="{{ url('profile')}}"><h3>Profile</h3></a></li>
            <li class="pb-3"><a href="{{ url('track')}}"><h3>Update My Tracker</h3></a></li>
            <li class="pb-3"><a href="{{ url('/home')}}"><h3>Dashboard</h3></a></li>
            <li class="pb-3"><a href="{{ url('/logout')}}"><h3>Logout</h3></a></li>
            @if(Auth::user() && Auth::user()->role_id == 1)
                <li class="pb-3"><a href="{{ url('/admin')}}"><h3>Admin Panel</h3></a></li>
            @endif
        </ul>
    </div>
    <div id="app">        
        <div class="container">
            <div class="row px-3 px-md-4">
                <div class="col-8 col-md-3 pt-3 pt-md-4 px-3 pl-md-4"><img src="{{ asset('img/mainlogo.svg') }}" alt="" width="300px"></div>
                <div class="col-4 col-md-3 text-center text-md-right offset-md-6 pt-2 px-2">
                    <a data-target="#helpModal" data-toggle="modal" href="#helpModal"><img class="img-fluid" src="{{ asset('img/living_smart-icons-08.png') }}" alt="" width="40px"></a>
                    <img class="img-fluid" src="{{ asset('img/living_smart-icons-10.png') }}" alt="" width="40px" id="openMenu">
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 offset-md-3 text-center">
                    @if(\Session::has('success'))
                        <div class="alert alert-success my-3 py-0">
                            <h4>{!! \Session::get('success') !!}</h4>
                        </div>
                    @endif
                </div>
            </div>
            <main class="py-3">
                @yield('content')
            </main>
        </div>
    </div>   
    @yield('scripts')
    <script>
        function open() {
          document.getElementById("sideMenu").style.display = "block";
        }
        
        function close() {
          document.getElementById("sideMenu").style.display = "none";
        }

        $(document).ready(function(){
            $("#closeMenu").click(function(){
                $("#sideMenu").hide('fast');
               
            });
            $("#openMenu").click(function(){
                $("#sideMenu").show('fast');                
            });
        });
    </script>
</body>

</html>
