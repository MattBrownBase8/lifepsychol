<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/img/smiley-face.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/img/smiley-face.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/img/smiley-face.png') }}">
    <!-- Scripts -->

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.2/main.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <link rel="stylesheet" type="text/css" href="{{asset('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}"/>
   <link rel="stylesheet" type="text/css" href="{{asset('lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
   <link rel="stylesheet" type="text/css" href="{{asset('lib/jquery.fullcalendar/fullcalendar.min.css') }}"/>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>   
    <div class="sideMenu pt-3" id="sideMenu" style="display: none">
        <div class="pr-3 text-right">
            <h1 id="closeMenu">x</h1>
        </div>
        <ul class="sideMenuList pr-4">
            <li class="pb-3"><a href="{{ url('/profile')}}"><h3>Profile</h3></a></li>
            <li class="pb-3"><a href="{{ url('track')}}"><h3>Update My Tracker</h3></a></li>
            <li class="pb-3"><a href="{{ url('/home')}}"><h3>Dashboard</h3></a></li>
            <li class="pb-3"><a href="{{ url('/logout')}}"><h3>Logout</h3></a></li>
            @if(Auth::user() && Auth::user()->role_id == 1)
                <li class="pb-3"><a href="{{ url('/admin')}}"><h3>Admin Panel</h3></a></li>
            @endif
        </ul>
    </div> 
    <div id="app">
        <div class="row px-1 px-md-4">
            <div class="col-8 col-md-3 pt-3 pt-md-4 px-3 pl-md-4">
                <img src="{{ asset('img/mainlogo.svg') }}" alt="" width="300px">
            </div>
            <div class="col-4 col-md-3 text-center text-md-right offset-md-6 pt-2 px-2">
                <a data-target="#helpModal" data-toggle="modal" href="#helpModal"><img class="img-fluid" src="{{ asset('img/living_smart-icons-08.png') }}" alt="" width="40px"></a>
                <img class="img-fluid" src="{{ asset('img/living_smart-icons-10.png') }}" alt="" width="40px" id="openMenu">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <main class="mb-5">
                    @yield('content')
                </main>
            </div>
        </div>
    </div>
</body>
@yield('script')
<script>
    function open() {
      document.getElementById("sideMenu").style.display = "block";
    }
    
    function close() {
      document.getElementById("sideMenu").style.display = "none";
    }

    $(document).ready(function(){
        $("#closeMenu").click(function(){
            $("#sideMenu").hide('fast');
           
        });
        $("#openMenu").click(function(){
            $("#sideMenu").show('fast');                
        });
    });
</script>
</html>
