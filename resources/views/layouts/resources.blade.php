<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>{{ config('app.name') }}</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="resourceBody">
  <div class="row">
    <div class="col-0 col-md-2">
      <div class="fixed-sidebar">
        <a href="#" class="js-close-fixed-sidebar pt-0">X</a>
        <div class="list-group list-group-flush resourceMenuItem">
          <h3 class="pt-3 text-center" style="color: white">Select Topic</h3>
          @foreach ($allCategories as $category)
            <a href="{{ $category->slug }}" class="py-2 px-3">{{ $category->name }}</a>
          @endforeach
          <a href="{{ url('/home') }}"><h3 class="pt-3 text-center" style="color: white">Back to Dashboard</h3></a>
        </div>
      </div>
    </div>
    <div class="col-12 col-md-10">
      <div id="app">    
        <main class="py-3">
          @yield('content')
        </main>    
      </div>
    </div>
  </div>
@yield('scripts')
<script>
  $(function() {
    $('.js-toggle-fixed-sidebar').click(function(e) {
      e.preventDefault();
      $('.fixed-sidebar').toggleClass('open');
    });

    $('.js-close-fixed-sidebar').click(function(e) {
      e.preventDefault();
      $('.fixed-sidebar').removeClass('open');
    })
  });
</script>
</body>
</html>
