<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">        
    </head>
    <body>
        <div class="full-height">
            <div class="container">
                <div class="row px-4 pt-5">
                    <div class="col-12">
                        <div class="content">
                            <div class="title m-b-md">
                                <img class="img-fluid pb-5" src="{{ asset('img/mainlogo.svg') }}" alt="" width="400px">
                            </div>
                            <form action="{{ route('addvalues.store')}}" method="POST" class="pb-2">
                                @csrf
                                <div class="row">
                                    <div class="col-12">
                                        <input class="form-control addEmotion" list="values" name="value">
                                        <datalist id="values" name="value">
                                            @foreach ($allValues as $value)
                                            <option>{{ $value->name }}</option>
                                            @endforeach
                                        </datalist >
                                    </div>
                                </div>                                
                                <div class="row">
                                    <div class="col-6">                                       
                                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-block loginButton mt-3" role="button" aria-pressed="true">Back</a>
                                    </div>
                                    <div class="col-6">
                                        <button class="btn btn-primary btn-block loginButton mt-3" type="submit">Add Value</button>
                                    </div>
                                </div>
                            </form> 
                            @if (\Session::has('error'))
                                    <div class="alert alert-danger py-1">
                                        {!! \Session::get('error') !!}
                                    </div>
                            @endif
                            <img class="img-fluid pt-5" src="{{ asset('img/LivingSmartTracker02.svg') }}" alt="">           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>