<div class="px-4" id="professionalData" style="display: none">
    <div class="userInstruction">
        <p>There are 3 easy steps to create your profile. After creating your profile, you can start using the tracker straight away.
        </p>
    </div>
    <form action="{{ url('/registerProfessional') }}" method="POST">
    @csrf        
        <div class="row">
            <div class="col-6 form-group">
                <label for="firstName" class="registerText mb-0">First Name</label>
                <input style="height:50px" type="text" name="firstName" class="form-control registerInputBoxes" required>
            </div>
            <div class="col-6 form-group">
                <label for="surname" class="registerText mb-0">Surname</label>
                <input style="height:50px" type="text" name="surname" class="form-control registerInputBoxes" required>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="email" class="registerText mb-0">Email</label>
                <input style="height:50px" type="email" name="email" class="form-control registerInputBoxes" required>
            </div>
        </div> 
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="code" class="registerText mb-0">Enter Company/Practice</label>
                <label for="code">If you do not belong to a company/practice, please leave blank</label>
                <input style="height:50px" type="text" name="company" class="form-control registerInputBoxes">
            </div>
        </div>          
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="professionalPassword1" class="registerText mb-0">Password</label>
                <input style="height:50px" type="password" id="professionalPassword1" name="professionalPassword1" class="form-control registerInputBoxes" required>
                <div class="pt-1" onclick="showProfPassword1()">click here to show/hide Password</div>
            </div>
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <label for="professionalPassword2" class="registerText mb-0">Retype Password <span style="display: none; font-size: 15px; color: red;" id="passwordWarning">Passwords Do Not Match</span></label>
                <input style="height:50px" type="password" id="professionalPassword2" name="professionalPassword2" class="form-control registerInputBoxes" required>
                <div class="pt-1" onclick="showProfPassword2()">click here to show/hide Password</div>
            </div>
        </div>            
        <div class="row pt-4">
            <div class="col-12 col-md-6 form-group">
                <div class="form-check pr-5">
                    <input type="checkbox" id="professionalConfirmTC" class="form-check-input registerInputRadio" name="optradio" style="vertical-align: middle">
                    <label class="form-check-label registerTextSmall" style="vertical-align: middle; padding-left: 20px; ">                      
                      <p>I have read and accept the <a href="{{ route('terms') }}">Terms and Conditions</a></p>
                    </label>
                </div>
            </div>             
        </div>
        <div class="row pt-4">
            <div class="col-12 form-group">
                <button  id="professionalNextButton" type="submit" class="btn btn-primary btn-block loginButton disabled" style="background-color: #344EA9; border: none;" disabled>Next</button >
            </div>
        </div>
    </form>
</div>