@extends('layouts.app', [
    'title' => 'Track Values'
])
@section('content')
<div class="container">
  <div class="row mb-2">
    <div class="col-12 text-center">
        <h1>Terms & Conditions</h1>       
    </div>
  </div>
</div>
<div class="px-4">
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pulvinar neque diam, a sodales nulla placerat in. Aliquam laoreet mollis malesuada. Duis at massa odio. In tincidunt eros sagittis tellus venenatis volutpat. Aenean eu mauris lorem. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris at felis vitae lectus lacinia facilisis eget nec dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam elementum, sapien luctus lacinia fermentum, lacus lorem faucibus tellus, sit amet rhoncus diam orci sed erat. Aenean eu libero nulla.</p>

  <p>Vestibulum sagittis sem ut erat rutrum, nec pretium elit feugiat. Proin eu dolor eget quam scelerisque fermentum. Proin semper interdum dapibus. Curabitur sagittis nisl ac augue accumsan, id ornare libero ultrices. Mauris malesuada pretium arcu ac mollis. Ut euismod purus in tempus convallis. Nullam non dui aliquam, finibus nunc quis, efficitur massa. Ut sed leo eget odio faucibus maximus ut sit amet lacus. Aenean vitae velit at tortor euismod sodales quis ut arcu. Maecenas ornare posuere arcu eget suscipit. Aenean sed lorem ante. Nulla interdum euismod massa eget maximus. Sed metus augue, ullamcorper nec mauris vel, interdum ultrices ipsum. Etiam tempus eros ut nisi rutrum viverra. Sed ullamcorper ut nunc vitae cursus. Vestibulum volutpat urna ipsum, a consectetur quam blandit ac.</p>
  
  <p>Cras tortor nibh, interdum non libero ac, pharetra vestibulum mi. Nullam id auctor orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean congue auctor urna, in congue urna fringilla ut. Nunc ut ante nisi. Vestibulum consectetur tortor quis diam convallis, et tincidunt tortor pellentesque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque consequat dui ut dui commodo, eget ullamcorper erat tempor. Praesent aliquam sit amet felis at malesuada. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis sed mauris dolor. Aenean pulvinar cursus erat, ut venenatis tellus tincidunt pharetra. Etiam luctus ipsum orci, nec ornare magna lobortis ut. Quisque fermentum lorem dictum fermentum dignissim.</p>
  
  <p>Aliquam pharetra mauris justo, quis dapibus tortor rhoncus at. In ultricies dignissim ipsum. Fusce vel risus tellus. Proin pellentesque vel augue ac venenatis. Pellentesque bibendum mollis molestie. Duis at dictum tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque congue libero nec tortor egestas, sed ornare dolor viverra. In ac nisi elit.</p>
  
  <p>Morbi vestibulum mauris eget leo vestibulum egestas. Etiam faucibus justo et nibh ultricies congue sit amet sit amet tortor. Integer fermentum eu mauris ut suscipit. Suspendisse potenti. Sed sit amet nunc tempor, pharetra elit quis, volutpat lacus. Aliquam non metus eget augue blandit pretium. Ut ullamcorper dui quis lacus cursus aliquet. Duis arcu lectus, tristique vitae facilisis vitae, porttitor eget lorem. Suspendisse felis augue, facilisis congue dolor et, convallis blandit enim. Etiam tempor felis vitae augue mollis pellentesque. Fusce porta nunc ultrices purus congue congue. Donec lectus diam, mattis ac lacus vitae, imperdiet laoreet erat. Phasellus molestie lobortis dolor, at cursus ipsum dignissim varius. Aenean nisl nibh, bibendum at placerat a, eleifend quis dui. Ut non imperdiet quam.</p>
</div>

@endsection
